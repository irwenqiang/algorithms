import math

def modexp(x, y, N):
	'''
	Input: Two n-bit integers x and N, an integer exponent y
	Output: x^y mod N
	log(N)
	'''

	if y == 0:
		return 1	

	z = modexp(x, math.floor(y / 2), N)
	
	if y % 2 == 0:
		return (z * z) % N
	else:
		return (x * z * z) % N
	

if __name__ == "__main__":
	print modexp(2, 5, 20)
