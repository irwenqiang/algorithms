import random

def primality(N):
	'''
		An algorithm for testing primality
		Pr(returns yes when N is prime) = 1
		Pr(returns yes when N is not prime) <= 1/2

		program accroding to Fermat's little theorem
	'''
	
	a = random.randrange(1, N)
	
	if pow(a,N-1) % N == 1:
		return "yes"
	else:
		return "no"
	
if __name__ == "__main__":
	print primality(101)
	print primality(341)
	print primality(587)
