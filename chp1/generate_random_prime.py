import random
import primality as p

def generate_random_prime(N):
	'''
		Input: prime number's bit number N
		Output: a prime number that is N-bit
		
		program according to Lagrange's prime number theorem: Let II(x) be the number of primes <= x, Then II(x) nearly equals to x / ln(x)
	'''
	flag = True
	while(flag):
		start = pow(2, N - 1)
		end = pow(2, N) - 1

		a = random.randrange(start, end)

		if p.primality(a) == "yes":
			flag = False
			print "find the first n-bit prime: ",
			print a
	
	
if __name__ == "__main__":
	generate_random_prime(3)
	generate_random_prime(10)
