import math

def extendEuclid(a, b):
	'''
		Input: two positive integers a and b with a >= b >= 0
		Output: Integers x, y, d such that d = gcd(a, b) and ax + by = d
	'''

	if b == 0:
		return (1, 0, a)
	(x, y, d) = extendEuclid(b, a % b)
	
	return (y, x - math.floor(a/b) * y, d)


if __name__ == "__main__":
	print extendEuclid(1035, 759)
