def Euclid(a, b):
	'''
		Euclid's rule
		O(n^3)
	'''
	if b == 0:
		return a

	return Euclid(b, a % b)

if __name__ == "__main__":
	print Euclid(1035, 759) 
