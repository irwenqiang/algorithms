/*
 * correct!
 * O(1.6^n)
 * there's better solution for the repeated computations
*/
#include <iostream>

using namespace std;

int fib0(int n) {
	if (n == 0)
		return 0;
	if (n == 1)
		return 1;
	
	return (fib0(n-1) + fib0(n-2));
}

int main() {

	int n = 5;
	cout << "fib: " << endl;
	cout << fib0(n) << endl;
	cout << fib0(9) << endl;
	cout << fib0(10) << endl;
	cout << fib0(15) << endl;
	cout << fib0(17) << endl;
	cout << fib0(20) << endl;
	
}
