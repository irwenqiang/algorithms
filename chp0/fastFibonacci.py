# http://nayuki.eigenstate.org/page/fast-fibonacci-algorithms

# returns F(n)
def fib(n):
	if n < 0:
		raise ValueError("Negative argument's not implemented")
	return _fib(n)[0];

# returns a tuple (F(n), F(n+!))
def _fib(n):
	if n == 0:
		return (0, 1)
	else:
		a, b = _fib(n / 2)
		c = a * (2 * b - a)
		d = b * b + a * a
		
		if n % 2 == 0:
			return (c, d)
		else:
			return (d, c + d)

if __name__ == "__main__":

	n = 9
	print fib(n)

	


