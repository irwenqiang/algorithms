/*
 * DP
 * O(n)
*/
#include <iostream>
using namespace std;

int fib1(int n) {
	if (n == 0)	return 0;	

	int* fibs = new int[n];

	fibs[0] = 0;
	fibs[1] = 1;

	for (int i = 2; i <= n; i++)
		fibs[i] = fibs[i - 1] + fibs[i - 2];

	return fibs[n];

}
int main() {
	cout << "polynomial fib: " << endl;
	
	cout << fib1(9) << endl;	
}
