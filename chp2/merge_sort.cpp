//http://www.personal.kent.edu/~rmuhamma/Algorithms/MyAlgorithms/Sorting/mergeSort.htm

#include <iostream>
#include <queue>

using namespace std;

void m_sort(int numbers[], int tmp[], int left, int right);
void merge(int numbers[], int tmp[], int left, int mid, int right);

queue<int> iter_merge_sort(int numbers[], int n) {
	queue<int> qs;
	for (int i = 0; i < n; i++) {
		qs.push(numbers[i]);
	}
	
	while(qs.size() > 1) {
		qs.push(merge(qs.pop(), qs.pop()));
	}
	
	return qs;
}

void merge_sort(int numbers[], int tmp[], int n) {
	m_sort(numbers, tmp, 0, n - 1);
}

void m_sort(int numbers[], int tmp[], int left, int right) {
	int mid;
		
	if (right > left) {
		mid = (right + left) >> 1;
		m_sort(numbers, tmp, left, mid);
		m_sort(numbers, tmp, mid+1, right);
		
		merge(numbers, tmp, left, mid+1, right);
	}
}

void merge(int numbers[], int tmp[], int left, int mid, int right) {
	int i, left_end, num_elements, tmp_pos;
	
	left_end = mid - 1;
	tmp_pos = left;
	num_elements = right - left + 1;
	
	while ((left <= left_end) && (mid <= right)) {
		if (numbers[left] <= numbers[mid]) {
			tmp[tmp_pos] = numbers[left];
			tmp_pos++;
			left++;
		}else {
			tmp[tmp_pos] = numbers[mid];
			tmp_pos++;
			mid++;
		}
	}
	
	while (left <= left_end) {
		tmp[tmp_pos] = numbers[left];
		tmp_pos++;	
		left++;
	}
	
	while (mid <= right) {
		tmp[tmp_pos] = numbers[mid];
		tmp_pos++;
		mid++;
	}
	
	for (int i = 0; i < num_elements; i++) {
		numbers[right] = tmp[right];	
		right--;
	}
}

int main() {
	int data[] = {44, 12, 145, -123, -1, 0, 121};
	int tmp[7];
	merge_sort(data, tmp, 7);
	
	for (int i = 0; i < 7; i++) 
		cout << data[i] << "\t";
	cout << endl;

	int data2[] = {44, 12, 145, -123, -1, 0, 121};

	queue<int> qs = iter_merge_sort(data, 7);


}
