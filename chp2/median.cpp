#include<iostream>
using namespace std;
 
/*
 *查找array数组里面array[left-right]第Max大的
 *参数：array  存储数据的数组
 *      left    数组的左界
 *      right   数组的右界
 *      Max     查找的第Max大
 */
void _f(int array[] , int left , int right , int Max){
    if(left>right){
        cout<<array[left+Max]<<endl;
        return;
    }
 
    int middle=array[left];
    int _left=left;
    int _right=right;
    while(_left<_right){
        while((_left<_right)&&(array[_right]>=middle)){
            _right--;
        }
        array[_left]=array[_right];
 
        while((_left<_right)&&(array[_left]<=middle)){
            _left++;
        }
        array[_right]=array[_left];
    }
 
    if((_left-left)==Max){
        cout<<array[_left]<<endl;
       return;
    }
 
    if((_left-left)<Max){
        _f(array , _left+1 , right , Max-(_left-left)-1);
    }else{
        _f(array , left , _left-1 , Max);
    }
}
 
 
/*
 *查找中位数
 *参数：   array   存储数据的数组
 *          length  数组的长度
 */
void f(int array[] , int length){
    _f(array , 0 , length-1 , length/2);
}
 
 
/*
 *测试用例
 */
int main(){
    int data[]={1,2,3,4,2,1,1,2,5,3,11, 12};
    f(data , 11);
	return 0;
}
