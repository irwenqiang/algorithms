#!/usr/bin/python
# -*- coding: utf-8 -*-

# refer: http://martin-thoma.com/strassen-algorithm-in-python-java-cpp/
from optparse import OptionParser
from math import ceil, log

def read(filename):
	lines = open(filename, 'r').read().splitlines()
	A = []
	B = []
	
	matrix = A
	
	for line in lines:
		if line != "":
			matrix.append(map(int, line.split("\t")))
		else:
			matrix = B

	return A, B

def printMatrix(matrix):
	for line in matrix:
		print "\t".join(map(str, line))

def ikjMatrixProduct(A, B):
	n = len(A)
	C = [[0 for i in xrange(n)] for j in xrange(n)]
	for i in xrange(n):
		for k in xrange(n):
			for j in xrange(n):
				C[i][j] += A[i][k] * B[k][j]
	return C

def add(A, B):
	n = len(A)
	C = [[0 for j in xrange(0, n)] for i in xrange(0, n)]
	for i in xrange(0, n):
		for j in xrange(0, n):
			C[i][j] += A[i][j] + B[i][j]
	return C

def subtract(A, B):
	n = len(A)
	C = [[0 for j in xrange(0, n)] for i in xrange(0, n)]
	for i in xrange(0, n):
		for j in xrange(0, n):
			C[i][j] = A[i][j] - B[i][j]
	return C

def strassenR(A, B):
	'''
		Implementation of the Strassen algorithm
	'''

	n = len(A)
	if n <= LEAF_SIZE:
		return ikjMatrixProduct(A, B)
	else:
		# initializing the new sub-matrices
		newSize = n/2
		a11 = [[0 for j in xrange(0, newSize)] for i in xrange(0, newSize)]
		a12 = [[0 for j in xrange(0, newSize)] for i in xrange(0, newSize)]
		a21 = [[0 for j in xrange(0, newSize)] for i in xrange(0, newSize)]
		a22 = [[0 for j in xrange(0, newSize)] for i in xrange(0, newSize)]

		b11 = [[0 for j in xrange(0, newSize)] for i in xrange(0, newSize)]
		b12 = [[0 for j in xrange(0, newSize)] for i in xrange(0, newSize)]
		b21 = [[0 for j in xrange(0, newSize)] for i in xrange(0, newSize)]
		b22 = [[0 for j in xrange(0, newSize)] for i in xrange(0, newSize)]

		aResult = [[0 for j in xrange(0, newSize)] for i in xrange(0, newSize)]
		bResult = [[0 for j in xrange(0, newSize)] for i in xrange(0, newSize)]
		
		# dividing the matrices in 4 sub-matrices
		for i in xrange(0, newSize):
			for j in xrange(0, newSize):
				a11[i][j] = A[i][j]			#top left
				a12[i][j] = A[i][j + newSize]		# top right
				a21[i][j] = A[i + newSize][j]		# bottom left
				a22[i][j] = A[i + newSize][j + newSize]	# bottom right
				
				b11[i][j] = B[i][j]			#top left
				b12[i][j] = B[i][j + newSize]		# top right
				b21[i][j] = B[i + newSize][j]		# bottom left
				b22[i][j] = B[i + newSize][j + newSize]	# bottom right

		# Calculating p1 to p7:
		aResult = add(a11, a22)
		bResult = add(b11, b22)
		
		p1 = strassenR(aResult, bResult)	# p1 = (a11 + a22) * (b11 + b22)
		
		aResult = add(a21, a22)			# a21 + a22
		p2 = strassenR(aResult, b11)		# p2 = (a21 + a22) * b11
		
		bResult = substract(b12, b22)		# b12 - b22
		p3 = strassenR(a11, bResult)		# p3 = (a11) * (b12 - b22)
		
		bResult = subtract(b21, b11) 		# b21 - b11
		
		... ... 
