#include <iostream>
using namespace std;

int binary(int array[], int n, int v) {
	int left = 0;
	int right = n;
	
	while (left < right) {
		int mid = left + ((right - left)>>1);

		if (array[mid] > v)
			right = mid;
		if (array[mid] < v)
			left = mid + 1;
		else
			return mid;
	}

	return -1;
	
}

int binary_search(int array[], int left, int right, int v) {
	
	int mid = left + ((right - left)>>1);
	
	if (left < right) {
		if (array[mid] > v)
			return binary_search(array, left, mid, v);
		else if (array[mid] < v) 
			return binary_search(array, mid + 1, right, v);
		else
			return mid;
		
	} 

	else if (left == right) {
		if (array[mid] == v) 
			return mid;
		return -1;
	}
	else 
		return -1;

	return -1;
}

int main() {
	int array[27] = {1, 3, 6, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 10, 20, 23, 23, 23, 199, 287};
	cout << "recursive: " << binary_search(array, 0, 27, 23) << endl;	
	cout << "iteration: " << binary(array, 27, 23) << endl;

	return 0;
		
}
