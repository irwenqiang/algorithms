// ref : http://www.programminglogic.com/knapsack-problem-dynamic-programming-algorithm/

#include <stdio.h>
#define max(a, b) (a > b ? a: b)

// 定义一个表，保存子问题
int matrix[100][100] = {0};

int knap(int index, int size, int weights[], int values[]) {
	int take, notTake;
	take = notTake = 0;

	if (matrix[index][size] != 0)
		return matrix[index][size];
	if (index == 0) {
		if (weights[0] <= size) {
			matrix[index][size] = values[0];
			return values[0];
		}else {
			matrix[index][size] = 0;
			return 0;
		}
	}

	if (weights[index] <= size) 
		take = values[index] + knap(index-1, size-weights[index], weights, values);

	notTake = knap(index - 1, size, weights, values);

	matrix[index][size] = max(take, notTake);

	return matrix[index][size];
}

int main() {
	int nItems = 4;
	int size = 10;
	int weights[4] = {5, 4, 6, 3};
	int values[4] = {10, 40, 30, 50};

	printf("Max value = %d\n", knap(nItems - 1, size, weights, values));

	return 0;
}
