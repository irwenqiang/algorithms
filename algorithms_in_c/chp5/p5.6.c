#include <stdio.h>

int imax(int a[], int l, int r) {
	int u, v;
	int m = (l + r) / 2;

	if (l == r)
		return a[l];

	u = imax(a, l, m);
	v = imax(a, m + 1, r);

	if (u > v)
		return u;
	else
		return v;
}

main(int argc, char *argv[]) {
	int a[5] = {4, 12, 24,53, 2};
	printf("%d\n", imax(a, 0, 4));
}
