#include <stdio.h>

int gcd(int m, int n) {
	if (n == 0)
		return m;
	else
		return gcd(n, m % n);
}

main(int argc, char *argv[]) {
	printf("%d\n", gcd(314159, 271828));
}
