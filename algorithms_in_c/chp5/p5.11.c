//dynamanic programming(memoization)
//难点在于使用memoization去解决子问题中的重复计算问题.
#include <stdio.h>

#define N 1000
int knownF[1000];

int F(int i) {
	int t;
	if (knownF[i] != -1)
		return knownF[i];
	if (i == 0)
		t = 0;
	if (i == 1)
		t = 1;
	if (i > 1) 
		t = F(i - 1) + F(i - 2);

	return knownF[i] = t;
}

int main(int argc, char* argv[]) {
	for (int i = 0; i < 1000; i++)
		knownF[i] = -1;

	int n = 5;
	printf("%d\n", F(7));
	printf("fibo(%d) = %d\n", n, knownF[n]);
}

