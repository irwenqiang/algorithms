#include <stdio.h>
void hanoi(char A, char B, char C, int n) {
	if (n == 1)
		printf("Movie disk %d from %c to %c\n", n, A, C);
	else {
		hanoi(A, C, B, n - 1);
		printf("Moive disk %d from %c to %c\n", n, A, C);
		hanoi(B, A, C, n - 1);
	}
}

int main(int argc, char* argv[]) {
	int n;
	printf("enter the order of hanoi\n");
	scanf("%d", &n);
	hanoi(n, 'A', 'B', 'C');
}
