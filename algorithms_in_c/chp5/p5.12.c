#include <stdio.h>

#define N 5
typedef struct {
	int size;
	int val;
} Item;

Item items[N];

int knap(int cap) {
	int i,t, max, space;
	for (i = 0, max = 0; i < N; i++) {
		if ((space = cap - items[i].size) >= 0) {
			if ((t = knap(space) + items[i].val) > max)
				max = t;
		}
	}

	return max;
}

int main(int argc, char* argv[]) {
	int i = 0;
	for (;i < N; i++) {
		items[i].size = i;
		items[i].val = i * 0.5;
	}
	knap(17);
}
