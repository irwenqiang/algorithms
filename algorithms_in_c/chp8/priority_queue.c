#include <stdlib.h>
#define key(A) A
#define less(A, B) (key(A) < key(B))
#define exch(A, B) {Item t = A; A = B; B = t;}

typedef int Item;
static Item *pq;
static int N;

void PQinit(int maxN) {
	pq = malloc(maxN * sizeof(Item));
	N = 0;
}

int PQempty() {
	return N == 0;
}

void PQinsert(Item v) {
	pq[N++] = v;
}

Item PQdelmax() {
	int j, max = 0;
	for (j = 1; j < N; j++) {
		if (less(pq[max], pq[j]))
			max = j;
	}
	exch(pq[max], pq[N-1]);
	return pq[--N];

}

int main(int argc, char* argv[]) {
	int n = 5;
	PQinit(5);
	PQinsert(6);
	PQinsert(2);
	printf("%d\n",PQdelmax());

}
