#include <stdlib.h>
#define key(A) A
#define less(A, B) (key(A) < key(B))
#define exch(A, B) {Item t = A; A = B; B = t;}

typedef int Item;

void fixDown(Item a[], int k, int N) {
	int j;
	while(2*k <= N) {
		j = 2 * k;
		if (j < N && less(a[j], a[j+1]))
			j++;
		if (!less(a[k], a[j]))
			break;

		exch(a[k], a[j]);
		k = j;
	}
}

void heapsort(Item a[], int l, int r) {
	int k, N = r - l + 1;
	Item* pq = a + l - 1;
	for (k = N / 2; k >= 1; k--)
		fixDown(pq, k, N);
	while(N > 1) {
		exch(pq[1], pq[N]);
		fixDown(pq, 1, --N);
	}

}
int main(int argc, char* argv[]) {
	int i;
       	int maxN = atoi(argv[1]);
	int *a = malloc(maxN * sizeof(int));

	for (i = 0; i < maxN; i++) { 
		a[i] = 1000 * (1.0 * rand() / RAND_MAX);
		printf("%d\t", a[i]);
	}
	printf("\n");
	heapsort(a, 0, maxN-1);

	for (i = 0; i <maxN; i++) 
		printf("%d\t", a[i]);

	printf("\n");

}
