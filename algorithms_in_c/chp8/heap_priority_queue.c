#include <stdlib.h>
#define key(A) A
#define less(A, B) (key(A) < key(B))
#define exch(A, B) {Item t = A; A = B; B = t;}

typedef int Item;
static Item *pq;
static int N;

void fixUp(Item a[], int k) {
	while(k > 1 && less(a[k/2], a[k])) {
		exch(a[k], a[k/2]);
		k = k / 2;
	}
}

void fixDown(Item a[], int k, int N) {
	int j;
	while(2*k <= N) {
		j = 2 * k;
		if (j < N && less(a[j], a[j+1]))
			j++;
		if (!less(a[k], a[j]))
			break;

		exch(a[k], a[j]);
		k = j;
	}
}

void PQinit(int maxN) {
	pq = malloc(maxN * sizeof(Item));
	N = 0;
}

int PQempty() {
	return N == 0;
}

void PQinsert(Item v) {
	pq[++N] = v;
	fixUp(pq, N);
}

Item PQdelmax() {
	exch(pq[1], pq[N]);
	fixDown(pq, 1, N-1);
	return pq[N--];
}

void PQsort(Item a[], int l, int r, int maxN) {
	int k;
	PQinit(maxN);
	for (k = l; k <= r; k++) PQinsert(a[k]);
	for (k = r; k >= l; k--) PQdelmax();
} 

int main(int argc, char* argv[]) {
	int i;
       	int maxN = atoi(argv[1]);
	int *a = malloc(maxN * sizeof(int));

	for (i = 0; i < maxN; i++) 
		a[i] = 1000 * (1.0 * rand() / RAND_MAX);
	PQsort(a, 1, maxN,maxN);

	for (i = 1; i <=maxN; i++) 
		printf("%d\t", pq[i]);

	printf("\n");

}
