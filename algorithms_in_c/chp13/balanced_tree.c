link balanceR(link h) {
	if (h->N < 2)
		return h;
	h = partR(h, h->N/2);
	h->l = balanceR(h->l);
	h->r = balanceR(h->r);

	return h;
}


link random_insertR(link h, Item item) {
	Key v = key(item);
	Key t = key(h->item);
	if (h == z)
		return NEW(item, z, z1);

	if (rand() < RAND_MAX/ (h->N + 1))
		return insertT(h, item);
	if less(v, t) 
		h->l = insertR(h->l, item);
	else
		h->r = insertR(h->r, item);

	(h->N)++;
	return h;
}

void STinsert(Item item) {
	head = insertR(head, item);
}
