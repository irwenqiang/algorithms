#include <stdlib.h>
#include "GRAPH_L.h"

#define maxV 1000
typedef struct node *link;

struct node {int v; link next;};
struct graph {int V; int E; link *adj;};

static int cnt, pre[maxV];
Edge EDGE(int v, int w) {
	Edge e = {v, w};
	return e;
}

link NEW(int v, link next) {
	link x = malloc(sizeof(*x));
	x->v = v;
	x->next = next;

	return x;
}

Graph GRAPHinit(int V) {
	int v;
	Graph G = malloc(sizeof(*G));
	G->V = V;
	G->E = 0;

	G->adj = malloc(V * sizeof(link));
	for (v = 0; v < V; v++)
		G->adj[v] = NULL;

	return G;
}

void GRAPHinsertE(Graph G, Edge e) {
	int v = e.v;
	int w = e.w;

	G->adj[v] = NEW(w, G->adj[v]);
	G->adj[w] = NEW(v, G->adj[w]);

	G->E++;
}

int GRAPHedges(Edge a[], Graph G) {
	int v;
	int E = 0;
	link t;
	for (v = 0; v < G->V; v++) {
		for (t = G->adj[v]; t != NULL; t = t->next)
			if (v < t->v)
				EDGE(v, t->v);
	}

	return E;
}

void GRAPHshow(Graph G) {
	//TODO
}
int randV(Graph G) {
	return G->V * (rand() / (RAND_MAX + 1.0));
}

Graph GRAPHrand(int V, int E) {
	Graph G = GRAPHinit(V);
	while(G->E < E) {
		GRAPHinsertE(G, EDGE(randV(G), randV(G)));
	}

	return G;
}

void dfsR4L(Graph G, Edge e) {
	link t;
	int w = e.w;
	pre[w] = cnt++;
	for (t = G->adj[w]; t != NULL; t = t->next) 
		if (pre[t->v] == -1)
			dfsR4L(G, t->v);
}

void GRAPHsearch(Graph G) {
	int v;
	cnt = 0;
	for (v = 0; v < G->V; v++)
		pre[v] = -1;
	for (v = 0; v < G->V; v++)
		if (pre[v] == -1)	
			dfsR4L(G);
	
	int i;
	for (i = 0; i < G->V; i++)
		printf("%d ", pre[i]);
}

void dfsRcc(Graph G, int v, int t) {
	link t;
	G->cc[v] = id;
	for (t = G->adj[v]; t != NULL; t = t->next)
		if (G->cc[t->v] == -1)
			dfsRcc(G, t->v, id);
}

void GRAPHcc(Graph G) {
	int v, id = 0;
	G->cc = malloc(G->V * sizeof(int));
	for (v = 0; v < G->V; v++) 
		G->cc[v] = -1;
	for (v = 0; v< G->V; v++) 
		if (G->cc[v] == -1)
			dfsRcc(G, v, id++);

	return id;
}

int GRAPHconnect(Graph G, int s, int t) {
	return G->cc[s] == G->cc[t];
}
