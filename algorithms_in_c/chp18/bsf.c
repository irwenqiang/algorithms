#define bsf search
void bfs(Graph G, Edge e) {
	int v, w;
	QUEUEput(e);

	while(!QUEUEempty())
		if (pre[(e = QUEUEget()).w] == -1) {
			pre[e.w] = cnt++;
			st[e.w] = e.v;

			for (v = 0; v < G->V; v++)
				if (G->adj[e.w][v] == 1)
					if (pre[v] == -1)
						QUEUEput(EDGE(e.w, v));
		}
}
