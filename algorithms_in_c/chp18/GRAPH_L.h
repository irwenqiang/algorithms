typedef struct {int v; int w;} Edge;

Edge EDGE(int, int);

typedef struct graph *Graph;

Graph GRAPHinit(int);
void GRAPHinsertE(Graph, Edge);
void GRAPHremoveE(Graph, Edge);
int GRAPHedges(Edge[], Graph G);
Graph GRAPHcopy(Graph);
void GRAPHdestroy(Graph);
void GRAPHshow(Graph);

Graph GRAPHrand(int, int);

void dfsR4L(Graph, Edge);
void GRAPHsearch(Graph);

void dfsRcc(Graph, int, int);

int GRAPHcc(Graph);

int GRAPHconnect(Graph, int, int);
