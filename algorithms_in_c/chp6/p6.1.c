#include <stdio.h>
#include <stdlib.h>

typedef int Item;

#define key(A) A
#define less(A, B) (key(A) < key(B))
#define exch(A, B) {Item t = A; A = B; B = t;}
#define compexch(A, B) if (less(A, B)) exch(A, B)

//---------------------------------------
// combine key for multi key sort
struct Tr {
	long date;
	long cust_id;

};

int compare(struct Tr *left, struct Tr *right) {
	long diff;
	// look at customer id first
	diff = right->cust_id - left->cust_id;
	if (diff != 0)
		return diff < 0 ? -1 : 1;
	// the cust_id match, so then check date
	diff = right->date - left->date;
	if (diff < 0)
		diff = diff <0 ? -1 : 1;

	return (int) diff;
}
//---------------------------------------
void sort(Item a[], int l, int r) {
	int i, j;
	for (i = l + 1; i <= r; i++)
		for (j = i; j > l; j--)
			compexch(a[j-1], a[j]);
}

void selection(Item a[], int l, int r) {
	int i, j;
	for (i = l; i < r; i++) {
		int min = i;
		for (j = i + 1; j <= r; j++) 
			if (less(a[j], a[min])) min = j;
		exch(a[i], a[min]);
	}
}

void insertion(Item a[], int l, int r) {
	int i;
	for (i = r; i > l; i--)
		compexch(a[i-1], a[i]);
	for (i = l + 1; i <= r; i++) {
		int j = i;
		Item v = a[i];
		while(!less(v, a[j-1])) {
			a[j] = a[j-1];
			j--;
		}

		a[j] = v;
	}
}

void bubble(Item a[], int l, int r) {
	int i, j;
	for (i = l; i < r; i++)
		for (j = r; j > i; j--) 
			compexch(a[j-1], a[j]);
}
main (int argc, char *argv[]) {
	int i, N = atoi(argv[1]);
	int *a = malloc(N * sizeof(int));

	for (i = 0; i < N; i++)
		a[i] = 1000 * ( 1.0 * rand() / RAND_MAX);

	//sort(a, 0, N-1);
	//selection(a, 0, N-1);
	insertion(a, 0, N - 1);
	//bubble(a, 0, N - 1);

	for (i = 0; i < N; i++)
		printf("%3d\t", a[i]);

	printf("\n");
}

