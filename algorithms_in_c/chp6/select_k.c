#include <stdio.h>
#include <stdlib.h>

typedef int Item;

#define key(A) A
#define less(A, B) (key(A) < key(B))
#define exch(A, B) {Item t = A; A = B; B = t;}
#define compexch(A, B) if (less(A, B)) exch(A, B)

int partion(Item a[], int l, int r) {
	int i = l - 1;
	int j = r;
	Item v = a[r];

	for (; ;) {
		while(less(a[++i], v));
		while(less(v, a[--j]))
			if (j == l)
				break;
		if ( i >= j)
			break;

		exch(a[i], a[j]);
	}

	exch(a[i], a[r]);

	return i;
}

int select_k(Item a[], int l, int r, int k) {
	int i;
	if (r <= l)
		return;
	i = partion(a, l, r);
	printf("i is : %d\n", i);
	if (i > k)
		select_k(a, l, i - 1, k);
	if (i < k)
		select_k(a, i + 1, r, k);
	else
		return a[i];
}

void quicksort(Item a[], int l , int r) {
	int i;
	if (r <= l)
		return;
	i = partion(a, l, r);
	quicksort(a, l, i - 1);
	quicksort(a, i + 1, r);
}

main (int argc, char *argv[]) {
	int i, N = atoi(argv[1]);
	int *a = malloc(N * sizeof(int));

	for (i = 0; i < N; i++){
		a[i] = 1000 * ( 1.0 * rand() / RAND_MAX);
		printf("%d\t", a[i]);
	}
		

	printf("\n");

	int kk = select_k(a, 0, N-1, 3);
	printf("%d", kk);

	
	printf("\n");
	quicksort(a, 0, N-1);
	for (i = 0; i < N; i++){
		
		printf("%d\t", a[i]);
	}
	printf("\n");

}


