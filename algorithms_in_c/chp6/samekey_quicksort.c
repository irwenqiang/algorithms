#include <stdio.h>
#include <stdlib.h>

typedef int Item;

#define key(A) A
#define less(A, B) (key(A) < key(B))
#define exch(A, B) {Item t = A; A = B; B = t;}
#define compexch(A, B) if (less(A, B)) exch(A, B)
#define eq(A, B) (!less(A, B) && !less(B, A))

int quicksort(Item a[], int l, int r) {
	int i, j, k, p, q;
	Item v = a[r];

	if ( r <= l)
		return;
	v = a[r];
	i = l - 1;
	j = r;

	p = l - 1;
	q = r;

	for (;;) {
		while(less(a[++i], v));
		while(less(v, a[--j]))
			if (j == l)
				break;

		if (i >= j)
			break;

		exch(a[i], a[j]);

		if (eq(a[i], v)) {
			p++;
			exch(a[p], a[i]);
		}
		if (eq(a[j], v)) {
			q--;
			exch(a[q], a[j]);
		}
	}

	exch(a[i], a[r]);
	j = i - 1;
	i = i + 1;

	for (k = l; k < p; k++, j--)
		exch(a[k], a[j]);
	for (k = r - 1; k > q; k--, i++) 
		exch(a[k], a[i]);

	quicksort(a, l, j);
	quicksort(a, i, r);
}

main (int argc, char *argv[]) {
	int i, N = atoi(argv[1]);
	int *a = malloc(N * sizeof(int));

	for (i = 0; i < N; i++)
		a[i] = 1000 * ( 1.0 * rand() / RAND_MAX);


	quicksort(a, 0, N - 1);

	for (i = 0; i < N; i++)
		printf("%3d\t", a[i]);

	printf("\n");
}

