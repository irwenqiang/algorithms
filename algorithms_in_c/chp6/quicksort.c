#include <stdio.h>
#include <stdlib.h>

typedef int Item;

#define key(A) A
#define less(A, B) (key(A) < key(B))
#define exch(A, B) {Item t = A; A = B; B = t;}
#define compexch(A, B) if (less(A, B)) exch(A, B)

int partion(Item a[], int l, int r) {
	int i = l - 1;
	int j = r;
	Item v = a[r];

	for (; ;) {
		while(less(a[++i], v));
		while(less(v, a[--j]))
			if (j == l)
				break;
		if ( i >= j)
			break;

		exch(a[i], a[j]);
	}

	exch(a[i], a[r]);

	return i;
}

int quicksort(Item a[], int l, int r) {
	if (r <= l)
		return;
	int i = 0;

	i = partion(a, l, r);

	quicksort(a, l, i - 1);
	quicksort(a, i + 1, r);
}

main (int argc, char *argv[]) {
	int i, N = atoi(argv[1]);
	int *a = malloc(N * sizeof(int));

	for (i = 0; i < N; i++)
		a[i] = 1000 * ( 1.0 * rand() / RAND_MAX);


	quicksort(a, 0, N - 1);

	for (i = 0; i < N; i++)
		printf("%3d\t", a[i]);

	printf("\n");
}

