link rotR(link h) {
	link x = h->l;
	h->l = x->r;
	x->r = h;

	return x;
}

link rotL(link h) {
	link x = h->r;
	h->r = x->l;
	x->l = h;

	return x;
}

link insertT(link h, Item item) {
	Key v = key(item);
	if (h == z)
		return NEW(item, z, z, 1);
	if (less(v, key(h->item))) {
		h->l = insertT(h->l, item);
		h = rotR(h);
	}else {
		h->r = insert(h->r, item);
		h=rotL(h);
	}

	return h;
}

void STinsert(Item item) {
	head = insertT(head, item);
}
