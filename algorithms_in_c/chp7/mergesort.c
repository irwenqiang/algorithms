#include <stdio.h>
#include <stdlib.h>

typedef int Item;

#define key(A) A
#define less(A, B) (key(A) < key(B))
#define exch(A, B) {Item t = A; A = B; B = t;}
#define compexch(A, B) if (less(A, B)) exch(A, B)
#define maxN 1000

void mergeAB(Item c[], Item a[], int N, Item b[], int M) {
	int i, j, k;
	for (i = 0, j = 0, k = 0; k < N + M; k++) {
		if (i == N) {
			c[k] = b[j++];
			continue;
		}
		if (j == M) {
			c[k] = a[i++];
			continue;
		}

		c[k] = (less(a[i], b[j])) ? a[i++] : b[j++];
	}
}

Item aux[maxN];
void merge(Item a[], int l, int m, int r) {
	int i, j, k;
	for(i = m + 1; i > l; i--)	
		aux[i - 1] = a[i - 1];
	for(j = m; j < r; j++)
		aux[r + m - j] = a[j + 1];

	for (k = l; k <= r; k++) 
		if(less(aux[j], aux[i]))
			a[k] = aux[j--];
		else
			a[k] = aux[i++];	
}

void mergesort(Item a[], int l, int r) {
	int m = (r + l) / 2;
	if (r <= l) return;
	mergesort(a, l, m);
	mergesort(a, m + 1, r);
	merge(a, l, m, r);
}

main (int argc, char *argv[]) {
	int i, N = atoi(argv[1]);
	int *a = malloc(N * sizeof(int));

	for (i = 0; i < N; i++)
		a[i] = 1000 * ( 1.0 * rand() / RAND_MAX);

	mergesort(a, 0, N - 1);

	for (i = 0; i < N; i++)
		printf("%3d\t", a[i]);

	printf("\n");
}

