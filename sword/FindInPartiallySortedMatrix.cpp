#include <iostream>
#include <string.h>
using namespace std;

bool Find(int* matrix, int rows, int columns, int number) {
	bool found = false;
	if (matrix != NULL && rows > 0 && columns > 0) {
		int row = 0;
		int column = columns - 1;
		while(row < rows && columns >= 0) {
			if (matrix[row * columns + column] == number) {
				found = true;
				break;
			}else if (matrix[row * columns + column] > number)
				--column;
			else
				++row;
		}
	}

	return found;
}

void testCase() {
	int matrix[][4] = {{1, 2, 8, 9}, {2, 4, 9, 12}, {4, 7, 10, 13}, {6, 8, 11, 15}};
	bool isFind = Find((int*)matrix, 4, 4, 7);
	cout << isFind << endl;
}
int main(int argc, char* argv[]){
	testCase();

	cout << "string test:" << endl;
	char str1[] = "hello irwenqiang";
	char str2[] = "hello irwenqiang";

	char* str3 = "hello irwenqiang";
	char* str4 = "hello irwenqiang";

	if (str1 == str2)
		cout << "str1 and str2 are the same" << endl;
	else
		cout << "str1 and str2 are not the same" << endl;

	if (str3 == str4)
		cout << "str3 and str4 are the same" << endl;
	else
		cout << "str3 and str4 are not the same" << endl;

	return 0;

}
