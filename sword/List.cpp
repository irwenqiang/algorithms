#include <stdio.h>
#include <stdlib.h>
#include "list.h"

ListNode* CreateListNode(int value) {
	ListNode* pNode = new ListNode();
	pNode->m_nValue = value;
	pNode->m_pNext = NULL;

	return pNode;
}

void ConnectListNodes(ListNode* pCurrent, ListNode* pNext) {
	if (pCurrent == NULL) {
		printf("Error to connect two nodes. pCurrent is NULL\n");
		return -1;
	}

	pCurrent->m_pNext = pNext;
}

void PrintListNode(ListNode* pNode) {
	if (pNode == NULL)
		printf("The node is NULL\n");
	else
		printf("The value in node is %d.\n", pNode->m_nValue);
}

void PrintList(ListNode* pHead) {
	if (pHead == NULL)
		printf("The List is NULL");
	else {
		ListNode* pNode = pHead;
		while(pNode != NULL) {
			printf("%d\t", pNode->m_nValue);
			pNode = pNode->m_pNext;
		}
	}
}

void DestroyList(ListNode* pHead) {
	if (pHead == NULL) 
		printf("The list is NULL");
	else {
		ListNode* pNode = pHead;
		while(pNode != NULL) {
			pHead = pHead->m_pNext;
			delete pNode;
			pNode = pHead;
		}
	}
}

void AddToTail(ListNode** pHead, int value) {
	ListNode* pNode = new ListNode();
	pNew->m_nValue = value;
	pNew->m_pNext = NULL;

	if (*pHead == NULL)
		*pHead = pNew;
	else {
		ListNode* pNode = *pHead;
		while(pNode->m_pNext != NULL)
			pNode = pNode->m_pNext;

		pNode->m_pNext = pNew;

	}
}

void RemoveNode(ListNode** pHead, int value) {
	if (pHead == NULL || *pHead == NULL)
		return;
	ListNode* pToBeDeleted = NULL;

	if ((*pHead)->m_nValue == value) {
		pToBeDeleted = *pHead;
		*pHead = (*pHead)->m_pNext;
	}else {
		ListNode* pNode = *pHead;
		while(pNode->m_pNext != NULL && pNode->m_pNext->m_nValue != value) {
			pNode = pNode->m_pNext;
		}

		if (pNode->m_pNext != NULL && pNode->m_pNext->m_nValue == value) {
			pToBeDeleted = pNode->m_pNext;
			pNode->m_pNext = pNode->m_pNext->m_pNext;
		}
	}

	if (pToBeDeleted != NULL) {
		delete pToBeDeleted;
		pToBeDeleted = NULL;
	}
}
