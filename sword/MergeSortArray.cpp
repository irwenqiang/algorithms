#include <iostream>
#include <string.h>

using namespace std;
int nL;
void MergeSortedArray(int a[], int lengthA, int b[], int lengthB) {
	if (a == NULL || lengthA <= 0 || b == NULL || lengthB <= 0)
		return;

	int nL = lengthA + lengthB;
	int nA[nL]; 
	
	while(lengthA >= 0 && lengthB >= 0) {
		if (a[lengthA - 1] > b[lengthB - 1]){
			nA[nL-1] = a[lengthA - 1];
			lengthA--;
		}
		else {
			nA[nL-1] = b[lengthB - 1];
			lengthB--;
		}

		nL--;
	}

	while(lengthA >= 0) {
		nA[nL--] = a[lengthA--];
	}

	while(lengthB >= 0) {
		nA[nL--] = b[lengthB--];
	}

	for (int i = 0; i < 10; i++)
		cout << nA[i] << "\t";

	cout << endl;	
}


int main(int argc, char* argv[]) {

	int a[] = {1, 3, 5, 7, 9};
	int b[] = {2, 4, 6, 8, 10};

	MergeSortedArray(a, 5, b, 5);	

	return 0;
}
