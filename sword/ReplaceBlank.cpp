#include <iostream>
#include <string.h>

using namespace std;

void ReplaceBlank(char string[], int length) {
	if (string == NULL || length <= 0)
		return;

	int originalLength = 0;
	int numberOfBlank = 0;
	int i = 0;

	while(string[i] != '\0') {
		++originalLength;
		if (string[i] == ' ')
			++numberOfBlank;
		++i;
	}

	int newLength = originalLength + numberOfBlank * 2;
	if (newLength > length)
		return;

	int indexOfOriginal = originalLength;
	int indexOfNew = newLength;

	while(indexOfOriginal >= 0 && indexOfNew > indexOfOriginal) {
		if (string[indexOfOriginal] == ' ') {
			string[indexOfNew--] = '0';
			string[indexOfNew--] = '2';
			string[indexOfNew--] = '%';
		}else {
			string[indexOfNew--] = string[indexOfOriginal];
		}

		indexOfOriginal--;
	}

}


int main(int argc, char* argv[]) {
	char string[1000] = "hello irwenqiang, you should to be a better man!";	
	ReplaceBlank(string, 1000);
	
	int i = 0;
	while(string[i] != '\0'){
		cout << string[i];
		i++;
	}

	cout << endl;
	return 0;
}
