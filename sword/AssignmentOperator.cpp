#include <iostream>
#include <string.h>
using namespace std;

class iString {
	public:
		iString(char *pData = NULL);
		iString(const iString& str);
		~iString(void);

		iString& operator = (const iString& str);

		void Print();
	private:
		char* m_pData;
};

iString::iString(char *pData) {
	if (pData == NULL){
		m_pData = new char[1];
		m_pData[0] = '\0';
	}else {
		int length = strlen(pData);
		m_pData = new char[length + 1];
		strcpy(m_pData, pData);
	}
}

iString::iString(const iString& str) {
	int length = strlen(str.m_pData);
	m_pData = new char[length + 1];
	strcpy(m_pData, str.m_pData);
}

iString::~iString(void) {
	delete[] m_pData;
}

// TODO page 26 has better solution
iString& iString::operator = (const iString& str) {
	if (this == &str)
		return *this;

	delete[] m_pData;
	
	m_pData = NULL;
	m_pData = new char[strlen(str.m_pData) + 1];
	strcpy(m_pData, str.m_pData);

	return *this;
}

void iString::Print() {
	cout << m_pData << endl;
}


int main(int argc, char* argv[]) {
	cout << "Test case:" << endl;
	char* text = "Hello irwenqiang";
	iString str1(text);
	iString str2, str3;
	str3 = str2 = str1;
	cout << "The expected result is: " << text << endl;
	cout << "The actual result is: ";
       	str2.Print();	
}
